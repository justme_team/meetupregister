﻿using System;
using System.Threading.Tasks;
using MeetupRegister.Data.Models;
using System.Collections.Generic;
using MeetupRegister.Core.Exceptions;
using MeetupRegister.Core.Abstractions.Services;
using MeetupRegister.Core.Abstractions.Repositories;
using System.Linq;

namespace MeetupRegister.Service.Services
{
    /// <summary>
    /// Attendee service implementation.
    /// </summary>
    public class AttendeeService : IAttendeeService
    {
        /// <summary>
        /// Attendee repository.
        /// </summary>
        private readonly IAttendeeRepository AttendeeRepository;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="attendeeRepository">Injected attendee repository.</param>
        public AttendeeService(IAttendeeRepository attendeeRepository)
        {
            AttendeeRepository = attendeeRepository;
        }

        /// <summary>
        /// Gets all registered attendees.
        /// </summary>
        /// <returns>A list of attendees.</returns>
        public async Task<IEnumerable<Attendee>> GetAll()
        {
            var result = await AttendeeRepository.GetAll();
            return result.ToList();
        }

        /// <summary>
        /// Registers a new attendee.
        /// </summary>
        /// <param name="newAttendee">Attendee to be registered.</param>
        /// <returns>Registered attendee.</returns>
        public async Task<Attendee> Register(Attendee newAttendee)
        {
            newAttendee.Id = Guid.NewGuid();
            if (AttendeeRepository.Exists(attendee => attendee.PersonalId == newAttendee.PersonalId || attendee.Email == newAttendee.Email))
                throw new AlreadyRegisteredException("Someone with the same Personal Id or Email address has already registered.");

            var addedAttendee = await AttendeeRepository.Register(newAttendee);
            return addedAttendee;
        }
    }
}
