﻿using MeetupRegister.Data;
using System.Threading.Tasks;
using System.Collections.Generic;
using MeetupRegister.Data.Models;
using MeetupRegister.Core.Abstractions.Repositories;

namespace MeetupRegister.Repository.Repositories
{
    /// <summary>
    /// Repository class for Attendees.
    /// </summary>
    public class AttendeeRepository : Repository<Attendee>, IAttendeeRepository
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="context">Injected database context.</param>
        public AttendeeRepository(AppDbContext context) : base(context)
        {
            Context = context;
        }

        /// <summary>
        /// Get all attendees registered.
        /// </summary>
        /// <returns>A list of all attendees.</returns>
        public async Task<IEnumerable<Attendee>> GetAll()
        {
            return await Get();
        }

        /// <summary>
        /// Adds a new attendee.
        /// </summary>
        /// <param name="attendee">Attendee to add.</param>
        /// <returns>Added attendee.</returns>
        public async Task<Attendee> Register(Attendee attendee)
        {
            var result = await Add(attendee);
            return result;
        }

        /// <summary>
        /// Finds an Attendee by personal id.
        /// </summary>
        /// <param name="personalid">Attendee's Personal id</param>
        /// <returns>Found attendee, if any.</returns>
        public Attendee FindByPersonalId(int personalId)
        {
            return Find(attendee => attendee.PersonalId == personalId);
        }
    }
}
