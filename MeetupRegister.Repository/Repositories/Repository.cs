﻿using System;
using System.Linq;
using MeetupRegister.Data;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using MeetupRegister.Core.Abstractions.Repositories;

namespace MeetupRegister.Repository.Repositories
{
    /// <summary>
    /// Generic repository implementation.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Database context.
        /// </summary>
        public DbContext Context;

        /// <summary>
        /// Repository constructor.
        /// </summary>
        /// <param name="context"></param>
        public Repository(AppDbContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Adds a new "entity" to the context.
        /// </summary>
        /// <param name="entity">Entity to save.</param>
        /// <returns>Saved entity</returns>
        public async Task<TEntity> Add(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
            await Context.SaveChangesAsync();
            return entity;
        }

        /// <summary>
        /// Gets all records of type "Entity".
        /// </summary>
        /// <returns>List of "entity"</returns>
        public async Task<IEnumerable<TEntity>> Get()
        {
            return await Context.Set<TEntity>().ToListAsync();
        }

        /// <summary>
        /// Finds a record of type "Entity" based on predicate.
        /// </summary>
        /// <param name="predicate">Predicate</param>
        /// <returns>Found entity.</returns>
        public TEntity Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate).First();
        }

        /// <summary>
        /// Checks whether an attendee exists or not based on personal id.
        /// </summary>
        /// <param name="predicate">Predicate</param>
        /// <returns>True if an attedee exists, false otherwise.</returns>
        public bool Exists(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Any(predicate);
        }
    }
}
