﻿using System.Threading.Tasks;
using MeetupRegister.Data.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;

namespace MeetupRegister.Core.Abstractions.Repositories
{
    public interface IAttendeeRepository
    {
        /// <summary>
        /// Gets all registered attendees.
        /// </summary>
        /// <returns>List of registered attendees.</returns>
        Task<IEnumerable<Attendee>> GetAll();

        /// <summary>
        /// Register a new attendee.
        /// </summary>
        /// <param name="attendee">Attendee to register.</param>
        /// <returns>Registered attendee.</returns>
        Task<Attendee> Register(Attendee attendee);

        /// <summary>
        /// Finds an Attendee by personal id.
        /// </summary>
        /// <param name="personalid">Attendee's Personal id</param>
        /// <returns>Found attendee, if any.</returns>
        Attendee FindByPersonalId(int personalId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        bool Exists(Expression<Func<Attendee, bool>> predicate);
    }
}
