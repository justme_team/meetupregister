﻿using System;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace MeetupRegister.Core.Abstractions.Repositories
{
    /// <summary>
    /// Generic Repository interface.
    /// </summary>
    /// <typeparam name="TEntity">Generic entity</typeparam>
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Gets all records of type "entity".
        /// </summary>
        /// <returns>List of entity</returns>
        Task<IEnumerable<TEntity>> Get();

        /// <summary>
        /// Adds a new "entity" to the context.
        /// </summary>
        /// <param name="entity">Entity to save</param>
        /// <returns>Saved entity</returns>
        Task<TEntity> Add(TEntity entity);

        /// <summary>
        /// Finds a record of type "Entity" based on predicate.
        /// </summary>
        /// <param name="predicate">Predicate</param>
        /// <returns>Found entity.</returns>
        TEntity Find(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Checks whether an attendee exists or not based on personal id.
        /// </summary>
        /// <param name="predicate">Predicate</param>
        /// <returns>True if an attedee exists, false otherwise.</returns>
        bool Exists(Expression<Func<TEntity, bool>> predicate);
    }
}
