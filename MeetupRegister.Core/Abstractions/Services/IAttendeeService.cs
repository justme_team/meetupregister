﻿using System.Threading.Tasks;
using MeetupRegister.Data.Models;
using System.Collections.Generic;

namespace MeetupRegister.Core.Abstractions.Services
{
    /// <summary>
    /// Interface definition for attendee service.
    /// </summary>
    public interface IAttendeeService
    {
        /// <summary>
        /// Gets all attendees.
        /// </summary>
        /// <returns>List of attendees.</returns>
        Task<IEnumerable<Attendee>> GetAll();

        /// <summary>
        /// Registers a new attendee.
        /// </summary>
        /// <returns>Newly-added attendee.</returns>
        Task<Attendee> Register(Attendee newAttendee);
    }
}
