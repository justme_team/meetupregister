﻿using MeetupRegister.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace MeetupRegister.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public virtual DbSet<Attendee> Attendees { get; set; }
    }
}