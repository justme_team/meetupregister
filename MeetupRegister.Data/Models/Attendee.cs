﻿using System;

namespace MeetupRegister.Data.Models
{
    public class Attendee
    {
        /// <summary>
        /// Gets or sets attendee's id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets attendees personal identifier.
        /// </summary>
        public int PersonalId { get; set; }

        /// <summary>
        /// Gets or sets attendee's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets attendee's last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets attendee's email addres.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets attendees phone number.
        /// </summary>
        public string PhoneNumber { get; set; }
    }
}
