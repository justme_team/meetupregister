using MeetupRegister.Data;
using MeetupRegister.Data.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using MeetupRegister.Service.Services;
using Microsoft.Extensions.Configuration;
using MeetupRegister.Repository.Repositories;
using Microsoft.Extensions.DependencyInjection;
using MeetupRegister.Core.Abstractions.Services;
using MeetupRegister.Core.Abstractions.Repositories;

namespace MeetupRegister
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<AppDbContext>(opts => opts.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<IRepository<Attendee>, Repository<Attendee>>();
            services.AddScoped<IAttendeeService, AttendeeService>();
            services.AddScoped<IAttendeeRepository, AttendeeRepository>();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",//TODO: Specify origin of the react site.
                builder =>
                {
                    builder.AllowAnyOrigin();
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors("CorsPolicy");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
