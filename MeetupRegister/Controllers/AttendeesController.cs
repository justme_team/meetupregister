﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using MeetupRegister.Data.Models;
using MeetupRegister.Core.Exceptions;
using MeetupRegister.Core.Abstractions.Services;

namespace MeetupRegister.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [EnableCors("CorsPolicy")]
    public class AttendeesController : ControllerBase
    {
        /// <summary>
        /// Attendee service.
        /// </summary>
        private readonly IAttendeeService AttendeeService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="attendeeService">Injected attendee service</param>
        public AttendeesController(IAttendeeService attendeeService)
        {
            AttendeeService = attendeeService;
        }

        /// <summary>
        /// Gets all attendees.
        /// </summary>
        /// <returns>List of attendees</returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await AttendeeService.GetAll();
            return Ok(result);
        }

        /// <summary>
        /// Creates a new attendee.
        /// </summary>
        /// <param name="newAttendee">Attendee to be created.</param>
        /// <returns>Created attendee.</returns>
        [HttpPost]
        public async Task<IActionResult> Post(Attendee newAttendee)
        {
            if (string.IsNullOrEmpty(newAttendee.Name) || string.IsNullOrEmpty(newAttendee.Email))
                return BadRequest("Please, provide your name and email.");

            try
            {
                var reusult = await AttendeeService.Register(newAttendee);
                return Ok(reusult);
            }
            catch (AlreadyRegisteredException)
            {
                return BadRequest("You have already registered.");
            }
        }
    }
}
